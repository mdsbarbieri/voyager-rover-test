import Rover from "./rover/Rover";
import * as readline from 'readline';
import {Plato} from "./types/Plato";


let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const readLine = (question: string): Promise<string> => {
    return new Promise((resolve) => {
        rl.question(question, (answer: string) => {
            resolve(answer)
        });
    })
}

let plato: Plato = {width: 10, height: 10};
const readPlatoSize = async (): Promise<void> => {
    console.log(`###################     INSERT PLATO SIZE IN FORMAT "H W"    #####################`)
    console.log(`###################     where H = height and W = width       #####################`)
    console.log(`################### if not set a plato of H=10 W=10 will be used  #####################`)
    const platoConfig = await readLine('Plato Size: ');
    if (!platoConfig) {
        return
    }
    const [height, width] = platoConfig.split(" ")
    if (!height || !width) {
        rl.close();
        throw new Error("Invalid plato data")
    }
    plato = {height: +height, width: +width}
}

const readData = async (): Promise<void> => {
    console.log(`-------------------------------------------------------------------------------`)
    const landing = await readLine('Landing Position: ');
    const instruction = await readLine('Instruction: ');

    if (!landing || !instruction) {
        return readData();
    }

    try {
        const [x, y, direction] = landing.split(" ")

        if (!x || !y || !direction) {
            throw new Error("Invalid input data")
        }

        const landPosition = {x: +x, y: +y, direction: direction.toUpperCase()}
        const rover = new Rover(plato, landPosition);
        const finalPosition = rover.sendRemoteCoordinates(instruction);

        console.log(`Final Position: ${finalPosition.x} ${finalPosition.y} ${finalPosition.direction}`)
        console.log(`-------------------------------------------------------------------------------`)
    } catch (e: any) {
        console.log(e.message || "Invalid input data")
    }

    const newCoordinate = await readLine('Send new coordinate? (Y/N): ');

    if (newCoordinate.toUpperCase() === 'Y') {
        return readData();
    }
}

readPlatoSize()
    .then(() => readData().finally(() => {
        rl.close()
    }))
    .catch(e => console.log(e.message || "Invalid data"))