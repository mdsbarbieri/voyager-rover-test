import Rover from "./Rover";

describe("Rover", () => {
    it("should turn rover east", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("R")
        expect(finalPosition.direction).toBe("E")

        finalPosition = rover.sendRemoteCoordinates("R")
        expect(finalPosition.direction).toBe("S")

        finalPosition = rover.sendRemoteCoordinates("R")
        expect(finalPosition.direction).toBe("W")

        finalPosition = rover.sendRemoteCoordinates("R")
        expect(finalPosition.direction).toBe("N")
    })

    it("should turn rover left", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("L")
        expect(finalPosition.direction).toBe("W")

        finalPosition = rover.sendRemoteCoordinates("L")
        expect(finalPosition.direction).toBe("S")

        finalPosition = rover.sendRemoteCoordinates("L")
        expect(finalPosition.direction).toBe("E")

        finalPosition = rover.sendRemoteCoordinates("L")
        expect(finalPosition.direction).toBe("N")
    })

    it("should move rover to north", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("M")
        expect(finalPosition.y).toBe(1)
    })

    it("should not move rover to north when in the border", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 2, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("M")
        expect(finalPosition.y).toBe(2)
    })

    it("should move rover to east", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("RM")
        expect(finalPosition.y).toBe(0)
        expect(finalPosition.x).toBe(1)
    })

    it("should not move rover to east when in the border", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 3, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("RM")
        expect(finalPosition.y).toBe(0)
        expect(finalPosition.x).toBe(3)
    })

    it("should move rover to west", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 1, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("LM")
        expect(finalPosition.y).toBe(0)
        expect(finalPosition.x).toBe(0)
    })

    it("should not move rover to west when in the border", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("LM")
        expect(finalPosition.y).toBe(0)
        expect(finalPosition.x).toBe(0)
    })

    it("should move rover to south", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 1, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("RRM")
        expect(finalPosition.y).toBe(0)
        expect(finalPosition.x).toBe(0)
    })

    it("should not move rover to south when in the border", () => {
        const rover = new Rover({width: 3, height: 2}, {x: 0, y: 0, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("RRM")
        expect(finalPosition.y).toBe(0)
        expect(finalPosition.x).toBe(0)
    })

    it("should result in the expected position", () => {
        const rover = new Rover({width: 10, height: 10}, {x: 1, y: 2, direction: 'N'});
        let finalPosition = rover.sendRemoteCoordinates("LMLMLMLMM")
        expect(finalPosition.x).toBe(1)
        expect(finalPosition.y).toBe(3)
        expect(finalPosition.direction).toBe("N")
    })

    it("should result in the expected position", () => {
        const rover = new Rover({width: 10, height: 10}, {x: 3, y: 3, direction: 'E'});
        let finalPosition = rover.sendRemoteCoordinates("MRRMMRMRRM")
        expect(finalPosition.x).toBe(2)
        expect(finalPosition.y).toBe(3)
        expect(finalPosition.direction).toBe("S")
    })

    it("should throw invalid landing position", () => {
        const outRange1 = () => {
            new Rover({width: 1, height: 1}, {x: 3, y: 3, direction: 'E'})
        };
        expect(outRange1).toThrow("Invalid landing position")
        const outRange2 = () => {
            new Rover({width: 1, height: 1}, {x: -3, y: -3, direction: 'E'})
        };
        expect(outRange2).toThrow("Invalid landing position")
    })

})