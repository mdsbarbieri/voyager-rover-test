import {Coordinates} from "../types/Coordinates";
import {Plato} from "../types/Plato";

class Rover {
    private currentPosition: Coordinates;
    private readonly positionArr = ["N", "E", "S", "W"]

    constructor(private readonly platoConfig: Plato, private readonly landingPosition: Coordinates) {
        if (this.isInvalidLandingPosition(platoConfig, landingPosition)) {
            throw new Error("Invalid landing position")
        }
        this.currentPosition = landingPosition;
    }

    private isInvalidLandingPosition(platoConfig: Plato, landingPosition: Coordinates) {
        return this.positionArr.indexOf(landingPosition.direction) < 0
            || landingPosition.y > platoConfig.height
            || landingPosition.x > platoConfig.width
            || landingPosition.x < 0
            || landingPosition.y < 0
    }

    private move() {
        switch (this.currentPosition.direction.toUpperCase()) {
            case "N":
                if (this.currentPosition.y == this.platoConfig.height) return;
                this.currentPosition.y += 1
                return
            case "E":
                if (this.currentPosition.x == this.platoConfig.width) return;
                this.currentPosition.x += 1
                return
            case "S":
                if (this.currentPosition.y == 0) return;
                this.currentPosition.y -= 1
                return
            case "W":
                if (this.currentPosition.x == 0) return;
                this.currentPosition.x -= 1
                return
        }
    }

    private toLeft() {
        const positionIndex = this.positionArr.indexOf(this.currentPosition.direction);
        if (positionIndex == 0) {
            this.currentPosition.direction = this.positionArr[this.positionArr.length - 1];
            return;
        }
        this.currentPosition.direction = this.positionArr[positionIndex - 1];
    }

    private toRight() {
        const positionIndex = this.positionArr.indexOf(this.currentPosition.direction);
        if (positionIndex == this.positionArr.length - 1) {
            this.currentPosition.direction = this.positionArr[0];
            return;
        }
        this.currentPosition.direction = this.positionArr[positionIndex + 1];
    }


    sendRemoteCoordinates(coordinates: string) {
        coordinates.toUpperCase().split("").forEach(cdt => {
            switch (cdt) {
                case "M":
                    this.move();
                    break
                case "L":
                    this.toLeft()
                    break
                case "R":
                    this.toRight()
                    break
            }
        })

        return this.currentPosition
    }
}

export default Rover