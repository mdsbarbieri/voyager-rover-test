export interface Coordinates{
    x: number,
    y: number,
    direction: string
}