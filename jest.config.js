module.exports = {
    clearMocks: true,
    coverageDirectory: "coverage",
    moduleFileExtensions: [ "ts", "js"],
    testEnvironment: "node",
    preset: 'ts-jest',
};