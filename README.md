# Mars Rover in JavaScript

### Install dependencies
```shell
yarn install
```
### Running Application
```shell
yarn start
```
### Running Tests
```shell
yarn test
```

#### Full Example of use

```shell
yarn start
$ ts-node src/app.ts
###################     INSERT PLATO SIZE IN FORMAT "H W"    #####################
###################     where H = height and W = width       #####################
################### if not set, a plato of H=10 W=10 will be used  #####################
Plato Size: 10 10
-------------------------------------------------------------------------------
Landing Position: 1 2 N
Instruction: LMLMLMLMM
Final Position: 1 3 N
-------------------------------------------------------------------------------
Send new coordinate? (Y/N): y
-------------------------------------------------------------------------------
Landing Position: 3 3 E
Instruction: MRRMMRMRRM
Final Position: 2 3 S
-------------------------------------------------------------------------------
Send new coordinate? (Y/N): n
```